const express = require("express")
const path = require('path')
const data = require('./jsonData')
const uuid = require('./uuid')



// initialize express
const app = express()


// create endpoints/route handlers

app.get('/', (req, res) => {
    // to send response 
    res.send(("Hello World, im express!"))
})

// GET /html 
app.get('/html', (req,res)=>{
    res.sendFile(path.join(__dirname, 'public' , '/index.html'))
})

// GET /json

app.get('/json', (req, res) => {
    res.json(data)
})


// GET /uuid 
app.get('/uuid', (req, res) => {
    res.json(uuid)
})

// Get status code



app.get('/status:status',(req,res)=>{
    res.status(req.params.status).send(`status code is ${req.params.status} `)
})


app.get('/delay/:seconds',(req,res)=>{
        setTimeout(() => {
            res.status(200).send(`request has succeeded. It took ${req.params.seconds} second` )
        }, (req.params.seconds)*1000);
})


const PORT = process.env.PORT || 5000
// listen to the port
app.listen(PORT, () => console.log(`server started on port ${PORT}`))